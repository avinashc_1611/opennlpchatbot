-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: nlp
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `item_name` varchar(100) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES ('Mobile',1),('Camera',2),('pendrive',3),('projector',4),('laptop',5),('watch',6);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_availability`
--

DROP TABLE IF EXISTS `item_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_availability` (
  `item_availability_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_location` varchar(45) DEFAULT NULL,
  `item_quantity` int(11) DEFAULT NULL,
  `item_lead_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_availability_id`),
  KEY `item_id_fk3_idx` (`item_id`),
  CONSTRAINT `item_id_fk3` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_availability`
--

LOCK TABLES `item_availability` WRITE;
/*!40000 ALTER TABLE `item_availability` DISABLE KEYS */;
INSERT INTO `item_availability` VALUES (1,1,'Bangalore',48,6),(2,1,'Hyderabad',10,6),(3,1,'Pune',20,6),(4,2,'Pune',0,6),(5,3,'Delhi',18,6),(6,2,'Chennai',82,6),(7,5,'Hyderabad',30,6),(8,6,'Chennai',19,6);
/*!40000 ALTER TABLE `item_availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_consumption`
--

DROP TABLE IF EXISTS `order_consumption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_consumption` (
  `order_cons_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_availibility_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_cons_id`),
  KEY `oc_order_idfk_idx` (`order_id`),
  KEY `oc_avial_id_fk_idx` (`item_availibility_id`),
  CONSTRAINT `oc_avial_id_fk` FOREIGN KEY (`item_availibility_id`) REFERENCES `item_availability` (`item_availability_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `oc_order_idfk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_consumption`
--

LOCK TABLES `order_consumption` WRITE;
/*!40000 ALTER TABLE `order_consumption` DISABLE KEYS */;
INSERT INTO `order_consumption` VALUES (2,2,6),(3,3,6);
/*!40000 ALTER TABLE `order_consumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_number` int(11) NOT NULL,
  `item_id` varchar(45) DEFAULT NULL,
  `order_qty` int(11) DEFAULT NULL,
  `expected_delivery_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_status` varchar(45) NOT NULL,
  PRIMARY KEY (`order_number`),
  KEY `item_id_fk_idx` (`item_id`),
  KEY `user_id_fk_idx` (`user_id`),
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'1',1,'2017-06-02 00:00:00',1,'CANCELLED'),(2,'2',1,'2017-06-02 00:00:00',1,'DISPATCHED'),(3,'2',1,'2017-06-02 00:00:00',1,'DISPATCHED');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Avi'),(2,'Gitu');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_chat_log`
--

DROP TABLE IF EXISTS `user_chat_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_chat_log` (
  `user_id` int(11) NOT NULL,
  `user_question` varchar(45) NOT NULL,
  `bot_answer` longtext NOT NULL,
  `positive` int(11) NOT NULL,
  `chatlogtime` datetime DEFAULT NULL,
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_chat_log`
--

LOCK TABLES `user_chat_log` WRITE;
/*!40000 ALTER TABLE `user_chat_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_chat_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`role`,`username`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (7,'avi','ADMIN'),(8,'gitu','ADMIN'),(9,'muskaan','ADMIN'),(4,'accountsadmin','ROLE_ACCOUNTS'),(5,'invadmin','ROLE_INVENTORY'),(6,'pubadmin','ROLE_PUBLICATION');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `userid` int(11) DEFAULT NULL,
  `userscol` varchar(45) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('accountsadmin','admin',1,NULL,''),('avi','abc123',1,1,''),('gitu','abc123',1,2,''),('invadmin','admin',1,NULL,''),('muskaan','abc123',1,3,''),('pubadmin','admin',1,NULL,'');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-29 10:37:28

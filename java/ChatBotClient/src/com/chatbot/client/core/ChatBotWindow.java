package com.chatbot.client.core;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.chatbot.util.UserContextInformation;


public class ChatBotWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtEnter = new JTextField();
	private JTextArea txtChat = new JTextArea();
	private ChatRestClient client = null;

	
	JScrollPane scroll = new JScrollPane (txtChat, 
			   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	

	private void initWindow(){
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setSize(600, 600);
				setVisible(true);
				setResizable(false);
				setLayout(null);
				setTitle("ORDERBOT");
				txtEnter.setLocation(2, 540);
				txtEnter.setSize(590, 30);
				txtChat.setLocation(15, 5);
				txtChat.setSize(560, 510);
				txtChat.setEditable(false);
				add(txtEnter);
				add(txtChat);
	}
	
	private void initAuth(){
		client = new ChatRestClient();
		boolean status = client.authenticateUser();
		if(!status){
			System.exit(1);
		}
	}
	
	public ChatBotWindow() {
		initAuth();
		initWindow();
		botSay("Hi, How may i help you?");
		txtEnter.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				String uText = txtEnter.getText();
				txtChat.append(client.getUserName() + ": " + uText + "\n");
				txtEnter.setText("");
				UserContextInformation information = client.chatWithBot(uText);
				botSay(information.getMessage());
				client.setUserContextInformation(information);
			}
		});
	}
	
	public void botSay(String s){
		txtChat.append("BOT: " + s + "\n");
	}

	public static void main(String[] args){
		new ChatBotWindow();
	}
}
package com.chatbot.client.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.chatbot.util.Reader;
import com.chatbot.util.UserContextInformation;

public class ChatRestClient {

	public ChatRestClient(){
		super();
		reader = new Reader();
		userContextInformation = new UserContextInformation();
		baseUrl = "http://"+reader.getHostName()+":"+reader.getPortNo()+"/"+reader.getServiceName()+"/";
	}
	
	private RestTemplate restTemplate = null;
	private String baseUrl = null;//"http://localhost:8080/ChatBotService/";
	private String loginPath = "login/";
	private String chatUrl = "chatWithUser/";
	private UserContextInformation userContextInformation = null;
	private Reader reader = null;
	private boolean isVerbose = false;
	private String userName = null;
	
	public boolean authenticateUser(){
		String uri = baseUrl + loginPath;
	    try{
	    	 userName = reader.getUserName();
	    	 String password = reader.getPassword();
	    	 String plainCreds = userName+":"+password;
			 byte[] plainCredsBytes = plainCreds.getBytes();
			 byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			 String base64Creds = new String(base64CredsBytes);
			 HttpHeaders headers = new HttpHeaders();
			 headers.add("Authorization", "Basic " + base64Creds);
			 headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
			 HttpEntity<Object> request = new HttpEntity<Object>(userName, headers);
	         restTemplate = getRestTemplate();
	         ResponseEntity<UserContextInformation>	 result = null;
			 result = restTemplate.exchange(uri, HttpMethod.POST, request, UserContextInformation.class); 
			 userContextInformation = result.getBody();
			 printMessage("User Logged In Successful");
	    }catch(ResourceAccessException ex){
	    	printMessage("Unable to Connect to the ChatBot Server");
	    	return false;
	    }catch(HttpClientErrorException ex){
	    	printMessage("Invalid Credentials");
	    	return false;
	    }
	    return true;
	}
	
	public String getUserName() {
		return userName;
	}

	public UserContextInformation chatWithBot(String message){
		 String uri = baseUrl + chatUrl;
		 String plainCreds = "avi:abc123";
		 byte[] plainCredsBytes = plainCreds.getBytes();
		 byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		 String base64Creds = new String(base64CredsBytes);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", "Basic " + base64Creds);
		 UserContextInformation sendU = new UserContextInformation(5);
		 if(userContextInformation!=null){
			 sendU.setMessage(message);
			 sendU.setUserId(userContextInformation.getUserId());
			 if(userContextInformation.getPreviousContext()!=null)
				 sendU.setPreviousContext(userContextInformation.getPreviousContext());	 
		 }
		 printMessage("Sending info object is" + sendU);
		 HttpEntity<UserContextInformation> request = new HttpEntity<UserContextInformation>(sendU, headers);
         ResponseEntity<UserContextInformation>	 result = null;
         restTemplate = getRestTemplate();
         result = restTemplate.exchange(uri, HttpMethod.POST, request, UserContextInformation.class);
         printMessage(result.getBody().getMessage());
         userContextInformation = result.getBody();
         return result.getBody();
	}
	
	
	private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(getMarshallingHttpMessageConverter());
        converters.add(new FormHttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }
 
    private MarshallingHttpMessageConverter getMarshallingHttpMessageConverter() {
 
        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter();
        marshallingHttpMessageConverter.setMarshaller(getJaxb2Marshaller());
        marshallingHttpMessageConverter.setUnmarshaller(getJaxb2Marshaller());
        return marshallingHttpMessageConverter;
    }
 
    private Jaxb2Marshaller getJaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        Class<?>[] listClass = new Class<?>[1];
        listClass[0] = UserContextInformation.class;
        jaxb2Marshaller.setClassesToBeBound(listClass);
        try {
			jaxb2Marshaller.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return jaxb2Marshaller;
    }

	public UserContextInformation getUserContextInformation() {
		return userContextInformation;
	}

	public void setUserContextInformation(UserContextInformation userContextInformation) {
		this.userContextInformation = userContextInformation;
	}
	
	private void printMessage(String message){
		if(isVerbose)
			System.out.println(message);
	}
}

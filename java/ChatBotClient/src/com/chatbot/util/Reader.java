package com.chatbot.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Reader {
	
	private String userName = null;
	private String password = null;
	private String hostName = null;
	private String portNo = null;
	private String serviceName = null;
	public Reader(){
		loadProperties();
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getPortNo() {
		return portNo;
	}

	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	private void loadProperties(){
		
		InputStream input = null;
		try {
			Properties prop = new Properties();
			input = new FileInputStream("botclient.properties");
			prop.load(input);
			userName = prop.getProperty("userName");
			password = prop.getProperty("password");
			hostName = prop.getProperty("hostName");
			portNo =   prop.getProperty("portNumber");
			serviceName = prop.getProperty("serviceName");
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String []args){
		new Reader();
	}
}


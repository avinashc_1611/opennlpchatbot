package com.iiit.nlp.model;

import java.io.File;

import opennlp.addons.modelbuilder.DefaultModelBuilderUtil;

public class DomainModel {

	private static String root = "D:\\Avinash\\IIIT\\Subjects\\NLP\\example\\opennlp-examples-master\\opennlp-examples-master\\src\\main\\resources\\";
	public static void main(String[] args) throws Exception {
		/**
	     * establish a file to put sentences in
	     */
	    File sentences = new File(root  + "actionmodel/sentences.txt");

	    /**
	     * establish a file to put your NER hits in (the ones you want to keep based on prob)
	     */
	    File knownEntities = new File(root  +"actionmodel/knownentities.txt");

	    /**
	     * establish a BLACKLIST file to put your bad NER hits in (also can be based on prob)
	     */
	    File blacklistedentities = new File(root  +"actionmodel/blentities.txt");

	    /**
	     * establish a file to write your annotated sentences to
	     */
	    File annotatedSentences = new File(root  +"actionmodel/annotatedSentences.txt");

	    /**
	     * establish a file to write your model to
	     */
	    File theModel = new File(root + "actionmodel/supplyChainAction.bin");

	    /**
	     * THIS IS WHERE THE ADDON IS GOING TO USE THE FILES (AS IS) TO CREATE A NEW MODEL. YOU SHOULD NOT HAVE TO RUN THE FIRST PART AGAIN AFTER THIS RUNS, JUST NOW PLAY WITH THE
	     * KNOWN ENTITIES AND BLACKLIST FILES AND RUN THE METHOD BELOW AGAIN UNTIL YOU GET SOME DECENT RESULTS (A DECENT MODEL OUT OF IT).
	     */
	    DefaultModelBuilderUtil.generateModel(sentences, knownEntities, blacklistedentities, theModel, annotatedSentences, "supplychain", 3);
	}
}

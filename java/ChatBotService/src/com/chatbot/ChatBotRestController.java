package com.chatbot;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.component.IOpenNLPService;
import com.chatbot.entity.UserContextInformation;

@RestController
public class ChatBotRestController {
	
	
	@Autowired
	private IOpenNLPService openNLPService;
	
	@PostConstruct
	public void init() {
		System.out.println(" *** ChatBotResController is initialized:");
	}
	
	@RequestMapping(value = "/chatWithUser", produces="application/xml", method = RequestMethod.POST)
	public UserContextInformation chatWithUser(@RequestBody UserContextInformation contextInformation) {
		//ChatBotAnswer answer = new ChatBotAnswer(contextInformation, openNLPService.getModel());
		UserContextInformation responseContextInformation = openNLPService.chatWithUser(contextInformation);
		return responseContextInformation;
	}
	
	@RequestMapping(value="/login",produces="application/xml",  method = RequestMethod.POST)
	public UserContextInformation authenticate(@RequestBody String userName) {
		System.out.println("Returning response");
		UserContextInformation contextInformation = openNLPService.getUserContextInformationOnSuccess(userName);
		return contextInformation;
	}
}
package com.chatbot.testclient;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;

import com.chatbot.entity.UserContextInformation;


public class TestClient {
	
	
    private RestTemplate restTemplate = null;
    private String loginUrl = "http://localhost:8080/ChatBotService/login";
    String chatUri = "http://localhost:8080/ChatBotService/chatWithUser";
	private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(getMarshallingHttpMessageConverter());
        converters.add(new FormHttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }
 
    /**
     * @return MarshallingHttpMessageConverter object which is responsible for
     *         marshalling and unMarshalling process
     */
    public MarshallingHttpMessageConverter getMarshallingHttpMessageConverter() {
 
        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter();
        marshallingHttpMessageConverter.setMarshaller(getJaxb2Marshaller());
        marshallingHttpMessageConverter.setUnmarshaller(getJaxb2Marshaller());
        return marshallingHttpMessageConverter;
    }
 
    /**
     * @return Jaxb2Marshaller, this is the Jaxb2Marshaller object
     */

    public Jaxb2Marshaller getJaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        Class<?>[] listClass = new Class<?>[1];
        listClass[0] = UserContextInformation.class;
        jaxb2Marshaller.setClassesToBeBound(listClass);
        try {
			jaxb2Marshaller.afterPropertiesSet();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return jaxb2Marshaller;
    }
	
	private UserContextInformation getEmployees(){
	    
	    UserContextInformation u = null;
	    //String loginuri = "http://localhost:8080/ChatBotService/login";
	    try{
	    	 String plainCreds = "avi:abc123";
			 byte[] plainCredsBytes = plainCreds.getBytes();
			 byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			 String base64Creds = new String(base64CredsBytes);
			 HttpHeaders headers = new HttpHeaders();
			 headers.add("Authorization", "Basic " + base64Creds);
			 headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
			 String userName = "avi";
			 HttpEntity<Object> request = new HttpEntity<Object>(userName, headers);
	         restTemplate = getRestTemplate();
	         ResponseEntity<UserContextInformation>	 result = null;
			 result = restTemplate.exchange(loginUrl, HttpMethod.POST, request, UserContextInformation.class); 
	         u = result.getBody();
	         System.out.println(u.getMessage());
	    }catch(Exception ex){
	    	ex.printStackTrace();
	    	return null;
	    }
	    return u;
	}
	
	/*private UserContextInformation postObject(String message, UserContextInformation previousInformation){
		
		 String plainCreds = "avi:abc123";
		 byte[] plainCredsBytes = plainCreds.getBytes();
		 byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		 String base64Creds = new String(base64CredsBytes);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", "Basic " + base64Creds);
		 UserContextInformation sendU = new UserContextInformation(5);
		 if(previousInformation!=null){
			 sendU.setMessage(message);
			 sendU.setUserId(previousInformation.getUserId());
			 if(previousInformation.getPreviousContext()!=null)
				 sendU.setPreviousContext(previousInformation.getPreviousContext());	 
		 }
		 System.out.println("Sending info object is" + sendU);
		 HttpEntity<UserContextInformation> request = new HttpEntity<UserContextInformation>(sendU, headers);
         ResponseEntity<UserContextInformation>	 result = null;
         restTemplate = getRestTemplate();
         result = restTemplate.exchange(chatUri, HttpMethod.POST, request, UserContextInformation.class);
         System.out.println(result.getBody().getMessage());
         return result.getBody();
	}
	
	private String getJSessionId(ResponseEntity<UserContextInformation> response){
		String str = response.getHeaders().get("Set-Cookie").get(0);
		String rec[] = str.split(";");
		int index = rec[0].indexOf("=");
		return rec[0].substring(index, rec[0].length());
		
	}
*/	
	public static void main(String []args){
		TestClient c = new TestClient();
		UserContextInformation info = c.getEmployees();
		System.out.println(info.getMessage());
		/*Scanner scanner = new Scanner(System.in);
		for(int i = 0;i<10;i++){
			String userInput = null;
			userInput = scanner.nextLine();
			info = c.postObject(userInput, info);
			System.out.println("Returned object is:"+info.toString());
		}
		scanner.close();*/
	}
}

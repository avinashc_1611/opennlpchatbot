package com.chatbot.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

@Component
public class ChatBotAuthenticationSuccessHandler implements AuthenticationSuccessHandler  {

	
	private RequestCache requestCache = new HttpSessionRequestCache();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {
		
		 SavedRequest savedRequest = requestCache.getRequest(request, response);
		 System.out.println("Saved request object is :"+savedRequest);

	}
	public void setRequestCache(final RequestCache requestCache) {
        this.requestCache = requestCache;
    }
}
package com.chatbot.component;

import com.chatbot.core.OpenNLPModelFactory;
import com.chatbot.entity.UserContextInformation;

public interface IOpenNLPService {

	public void callModel();
	
	public OpenNLPModelFactory getModel();
	
	public UserContextInformation getUserContextInformationOnSuccess(String userName);
	
	public UserContextInformation chatWithUser(UserContextInformation information);
		
}

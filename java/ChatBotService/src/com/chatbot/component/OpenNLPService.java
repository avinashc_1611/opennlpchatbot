package com.chatbot.component;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chatbot.core.ChatBotAnswer;
//import com.chatbot.core.DBUtil;
import com.chatbot.core.OpenNLPModelFactory;
import com.chatbot.dao.ChatBotDAO;
import com.chatbot.entity.UserContextInformation;

@Component
public class OpenNLPService implements IOpenNLPService {

	@Autowired
	OpenNLPModelFactory model = null;
	
	@Autowired
	private ChatBotDAO chatBotDao;
	
	public OpenNLPService(){
			super();
			System.out.println("Loading Open NLP Service");
	}
	
	public OpenNLPModelFactory getModel() {
		return model;
	}
	public void setModel(OpenNLPModelFactory model) {
		this.model = model;
	}
	@Override
	public void callModel() {
		//System.out.println("call model is called:"+model.hashCode());
	}
	
	@Override
	public UserContextInformation getUserContextInformationOnSuccess(String userName){
		int userId = 0;
		//DBUtil util = new DBUtil();
		//userId = util.getUserId(userName);
		userId = chatBotDao.getUserId(userName);
		return new UserContextInformation(userId);
	}

	@Override
	public UserContextInformation chatWithUser(UserContextInformation information) {
		ChatBotAnswer answer = new ChatBotAnswer(information, getModel(), chatBotDao);
		UserContextInformation responseContextInformation = answer.respondUser();
		return responseContextInformation;
	}
}

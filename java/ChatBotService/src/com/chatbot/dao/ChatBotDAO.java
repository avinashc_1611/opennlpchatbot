package com.chatbot.dao;

import java.util.List;

import com.chatbot.entity.Order;

public interface ChatBotDAO {
	
	public int getUserId(String userName);
	
	public boolean logChatWithUser(int userId, String userQuery, String botAnswer, byte helpful);
	
	public Order cancelOrder(String orderNumberStr);
	
	public Order placeOrder(String itemName, int qty, int userId);
	
	public List<Order> getAllOrderStatus(int userId);

}

package com.chatbot.config;  
  
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.chatbot.core.OpenNLPModelFactory;
import com.chatbot.entity.UserContextInformation;


@EnableWebMvc
@Configuration 
@ComponentScan(basePackages = { "com.chatbot","com.chatbot.security","com.chatbot.dao","com.chatbot.config"}) 
public class ChatBotInitialConfig {  

	@Bean
	 public OpenNLPModelFactory getOpenNLPModelFactory() {
		 OpenNLPModelFactory factory = new OpenNLPModelFactory();
	     return factory;
	 }
	
	@Bean
    public RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(getMarshallingHttpMessageConverter());
        converters.add(new FormHttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }
 
    @Bean
    public MarshallingHttpMessageConverter getMarshallingHttpMessageConverter() {
 
        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter();
        marshallingHttpMessageConverter.setMarshaller(getJaxb2Marshaller());
        marshallingHttpMessageConverter.setUnmarshaller(getJaxb2Marshaller());
        return marshallingHttpMessageConverter;
    }
 
    @Bean
    public Jaxb2Marshaller getJaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        Class<?>[] listClass = new Class<?>[1];
        listClass[0] = UserContextInformation.class;
        jaxb2Marshaller.setClassesToBeBound(listClass);
        return jaxb2Marshaller;
    }
}  
